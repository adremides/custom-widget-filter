from .models import Registro
from .forms import RegistroForm
from django import forms
from cruds_adminlte.crud import CRUDView
from cruds_adminlte.filter import FormFilter
from cruds_adminlte import (DatePickerWidget)


class RangoFechaForm(forms.Form):
    created_date__gt = forms.DateField(
        label='Fecha desde',
        widget=DatePickerWidget(attrs={
            'format': 'dd/mm/yyyy',
            'icon': 'fa fa-calendar'
        })
    )
    created_date__lt = forms.DateField(
        label='Fecha hasta',
        widget=forms.DateInput(attrs={'placeholder': 'DD/MM/YYYY', }),
    )


class RangoFechaFilter(FormFilter):
    form = RangoFechaForm


class RegistrosCRUD(CRUDView):
    model = Registro
    check_login = False
    check_perms = False
    views_available = ['create', 'list', 'delete', 'update']
    add_form = RegistroForm
    list_filter = [
        RangoFechaFilter,
    ]
