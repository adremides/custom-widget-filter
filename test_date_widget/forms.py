from django import forms
from .models import Registro
from cruds_adminlte import (DatePickerWidget)


class RegistroForm(forms.ModelForm):

    class Meta:
        model = Registro
        fields = [
            'fecha',
            'texto',
        ]

        widgets = {
            'fecha': DatePickerWidget()
        }
