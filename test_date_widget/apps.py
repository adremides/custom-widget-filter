from django.apps import AppConfig


class TestDateWidgetConfig(AppConfig):
    name = 'test_date_widget'
