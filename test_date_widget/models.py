from django.db import models


class Registro(models.Model):
    fecha = models.DateField('Fecha')
    texto = models.TextField(max_length=50)
