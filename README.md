# custom widget filter problem

Install requirements.txt
``` bash
pip install -r requirements.txt
```

To start project run 
``` bash
python manage.py runserver
```

Go to http://127.0.0.1:8000/test_date_widget/registro/list

Widget is working correctly when we add a new item  
![widget_working_correcly](creation_fine.png "widget working correctly")

When we use it in a filter, widget load but it didn't work fine. It didn't show the calendar to pick the date  
![widget_working_badly](filter_wrong.png "widget working badly")
